articuloArticulo<?php
  class Articulo extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Insertar nuevos hospitales
    function insertar($datos){
      $respuesta=$this->db->insert("articulo",$datos);
      return $respuesta;
    }
    //Consulta de datos
    function consultarTodos(){
      $revistas=$this->db->get("articulo");
      if ($revistas->num_rows()>0) {
        return $revistas->result();
      } else {
        return false;
      }
    }


    public function consultarTodosConEditoriales() {
        $this->db->select('articulo.*, revista.nombre AS nombre_revista');
        $this->db->from('articulo');
        $this->db->join('revista', 'articulo.fkid_revis = revista.id', 'left');
        $query = $this->db->get();
        return $query->result();
    }


    // Obtener hospital por ID
function obtenerPorId($id)
{
    $this->db->where("id", $id);
    $articulo = $this->db->get("articulo");
    if ($articulo->num_rows() > 0) {
        return $articulo->row();
    } else {
        return false;
    }
}



    //eliminacion de hospital por id
    function eliminar($id){
        $this->db->where("id",$id);
        return $this->db->delete("articulo");
    }

    //funcion para actualizar hospitales
function actualizar($id,$datos){
  $this->db->where("id",$id);
  return $this->db->update("articulo",$datos);
}




function obtenerListadoRevistas()
   {
       $revistas = $this->db->get("revista")->result();
       return $revistas;
   }


  }//Fin de la clase



?>
