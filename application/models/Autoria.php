<?php
  class Autoria extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Insertar nuevos hospitales
    function insertar($datos){
      $respuesta=$this->db->insert("autoria",$datos);
      return $respuesta;
    }
    //Consulta de datos
    function consultarTodos(){
      $autorias=$this->db->get("autoria");
      if ($autorias->num_rows()>0) {
        return $autorias->result();
      } else {
        return false;
      }
    }

    function consultarTodosConAutoresYArticulos() {
        $query = $this->db->query('SELECT autoria.*, articulo.nombre AS nombre_articulo, autor.nombre AS nombre_autor FROM autoria LEFT JOIN articulo ON autoria.fkid_arti = articulo.id LEFT JOIN autor ON autoria.fkid_autor = autor.id');
        return $query->result();
    }


    public function consultarTodosConArticulos() {
        $this->db->select('autoria.*, articulo.nombre AS nombre_articulo');
        $this->db->from('autoria');
        $this->db->join('articulo', 'autoria.fkid_arti = articulo.id', 'left');
        $query = $this->db->get();
        return $query->result();
    }

    public function consultarTodosConAutores() {
        $this->db->select('autoria.*, autor.nombre AS nombre_autor');
        $this->db->from('autoria');
        $this->db->join('autor', 'autoria.fkid_autor = autor.id', 'left');
        $query = $this->db->get();
        return $query->result();
    }

    // Obtener hospital por ID
function obtenerPorId($id)
{
    $this->db->where("id", $id);
    $autoria = $this->db->get("autoria");
    if ($autoria->num_rows() > 0) {
        return $autoria->row();
    } else {
        return false;
    }
}



    //eliminacion de hospital por id
    function eliminar($id){
        $this->db->where("id",$id);
        return $this->db->delete("autoria");
    }

    //funcion para actualizar hospitales
function actualizar($id,$datos){
  $this->db->where("id",$id);
  return $this->db->update("autoria",$datos);
}




function obtenerListadoAutores()
{
    $autores = $this->db->get("autor")->result();
    return $autores;
}



   function obtenerListadoArticulos()
   {
       $articulos = $this->db->get("articulo")->result();
       return $articulos;
   }


  }//Fin de la clase



?>
