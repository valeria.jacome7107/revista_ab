<?php
    class Director extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Insertar nuevos hospitales
    
    function insertar($datos){
      // Utiliza el método set() para establecer los datos que deseas insertar
      $this->db->set($datos);

      // Luego, realiza la inserción en la tabla 'editorial'
      $respuesta = $this->db->insert("director");

      return $respuesta;
  }
    //Consulta de datos
    function consultarTodos(){
      $directores=$this->db->get("director");
      if ($directores->num_rows()>0) {
        return $directores->result();
      } else {
        return false;
      }
    }

    public function consultarTodosConEditoriales() {
        $this->db->select('director.*, editorial.nombre AS nombre_editorial');
        $this->db->from('director');
        $this->db->join('editorial', 'director.fkid_edi = editorial.id', 'left');
        $query = $this->db->get();
        return $query->result();
    }




    // Obtener hospital por ID
function obtenerPorId($id)
{
    $this->db->where("id", $id);
    $director = $this->db->get("director");
    if ($director->num_rows() > 0) {
        return $director->row();
    } else {
        return false;
    }
}



    //eliminacion de hospital por id
    function eliminar($id){
        $this->db->where("id",$id);
        return $this->db->delete("director");
    }

    //funcion para actualizar hospitales
function actualizar($id,$datos){
  $this->db->where("id",$id);
  return $this->db->update("director",$datos);
}

function obtenerListadoEditoriales()
   {
       $editoriales = $this->db->get("editorial")->result();
       return $editoriales;
   }

  }//Fin de la clase
?>
