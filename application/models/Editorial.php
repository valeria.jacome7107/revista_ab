<?php
    class Editorial extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Insertar nuevos hospitales
    function insertar($datos){
      // Utiliza el método set() para establecer los datos que deseas insertar
      $this->db->set($datos);

      // Luego, realiza la inserción en la tabla 'editorial'
      $respuesta = $this->db->insert("editorial");

      return $respuesta;
  }


    //Consulta de datos
    function consultarTodos(){
      $editoriales=$this->db->get("editorial");
      if ($editoriales->num_rows()>0) {
        return $editoriales->result();
      } else {
        return false;
      }
    }


function obtenerPorId($id)
{
    $this->db->where("id", $id);
    $editorial = $this->db->get("editorial");
    if ($editorial->num_rows() > 0) {
        return $editorial->row();
    } else {
        return false;
    }
}

    function eliminar($id){
        $this->db->where("id",$id);
        return $this->db->delete("editorial");
    }

function actualizar($id,$datos){
  $this->db->where("id",$id);
  return $this->db->update("editorial",$datos);
}


  }//Fin de la clase
?>
