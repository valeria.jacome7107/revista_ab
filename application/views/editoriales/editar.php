<div style="padding: 150px 70px 20px 100px">

    <h1>
        <b>
            <i class="fa-solid fa-pen-to-square"></i>
            EDITAR EDITORIAL
        </b>
    </h1>
    <br>

    <form class="text-dark" action="<?php echo site_url('editoriales/actualizarEditorial'); ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_editorial">

        <input type="hidden" value="<?php echo $editorialEditar->id; ?>" name="id" id="id">

        <div class="mb-3">
            <label for="nombre" class="form-label"><b>Nombre:</b></label>
            <input id="nombre" type="text" name="nombre" value="<?php echo $editorialEditar->nombre; ?>" oninput="EspacioLetrasNumeros(this)" placeholder="Ingrese el nombre de la editorial" class="form-control text-dark" required>
        </div>
        <div class="mb-3">
            <label for="telefono" class="form-label"><b>Teléfono:</b></label>
            <input id="telefono" type="text" name="telefono" value="<?php echo $editorialEditar->telefono; ?>" oninput="validarNumeros(this)" placeholder="Ingrese el teléfono" class="form-control text-dark" required>
        </div>
        <div class="mb-3">
            <label for="correo" class="form-label"><b>Correo:</b></label>
            <input id="correo" type="email" name="correo" value="<?php echo $editorialEditar->correo; ?>" oninput="" placeholder="Ingrese el correo" class="form-control text-dark" required>
        </div>

        <br>
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-warning"> <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbspActualizar&nbsp</button>
                &nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a class="btn btn-danger" href=" <?php echo site_url('editoriales/index') ?> "><i class="fa-solid fa-xmark  fa-spin"></i>&nbspCancelar&nbsp</a>
            </div>

        </div>

    </form>

</div>

<script type="text/javascript">
function validarLetras(input) {
  input.value = input.value.replace(/\s+/g, ' ').replace(/[^a-zA-ZñÑ\s]/g, '');
  input.value = input.value.toUpperCase();

}


function validarNumeros(input) {
input.value = input.value.replace(/\D/g, '');
}

</script>
<script type="text/javascript">
$(document).ready(function() {
    $("#frm_nuevo_editorial").validate({
        rules: {

            "nombre": {
                required: true,
                minlength: 5

            },
            "telefono": {
              required: true,
              minlength: 7, // Puedes ajustar la longitud según tus necesidades
              maxlength: 10,
              pattern: /^\+?([0-9]{1,3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/

            },

            "correo": {
              required: true,
              email: true

            }


        },
        messages: {

            "nombre": {
                required: "Debe ingresar el nombre de la editorial",
                minlength: "El nombre no debe tener menos de 5 caracteres"

            },
            "telefono": {
              required: "Por favor, ingresa tu número de teléfono",
               minlength: "El número de teléfono debe tener al menos 7 dígitos",
               maxlength: "El número de teléfono no puede tener más de 10 dígitos",
               pattern: "Por favor, ingresa un número de teléfono válido"
            },

            "correo": {
              required: "Por favor, ingresa tu correo electrónico",
              email: "Por favor, ingresa un correo electrónico válido"
            }




        }
    });
});


</script>
