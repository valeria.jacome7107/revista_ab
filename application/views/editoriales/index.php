<div style="padding: 150px 70px 20px 100px">
     <div class="text-center">
         <h1><i class="fa-solid fa-book"></i>&nbsp;&nbsp;Editoriales</h1>
    </div>
    <div class="row">
    <div class="col-md-12 text-end">

      <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
         <i class="fa fa-plus-circle fa-1x"></i> Agregar editorial
      </button>


    </div>

  </div><br>


  <?php if ($listadoEditoriales): ?>
    <table class="table table-striped text-center">
    <thead class="table-dark">
        <tr>
            <th>ID</th>
            <th>NOMBRE</th>
            <th>TELEFONO</th>
            <th>CORREO</th>
            <th>ACCIONES</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($listadoEditoriales as $editorial): ?>
            <tr>
                <td class="text-dark"><?php echo $editorial->id; ?></td>
                <td class="text-dark"><?php echo $editorial->nombre; ?></td>
                <td class="text-dark"><?php echo $editorial->telefono; ?></td>
                <td class="text-dark"><?php echo $editorial->correo; ?></td>
                <td>
                    <a href="<?php echo site_url('editoriales/editar/').$editorial->id; ?>" class="btn btn-warning" title="Editar">
                        <i class="fa fa-pen"></i>
                    </a>
                    <a href="#" class="btn btn-danger" onclick="eliminarRegistro('<?php echo site_url('editoriales/borrar/').$editorial->id; ?>')">
                        <i class="fa fa-trash"></i>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>


  <div class="modal-footer">

  </div>

              </div>
            </div>
          </div>
    <?php else: ?>
          <div class="alert alert-danger">
              No se encontro editoriales registrados
          </div>
  <?php endif; ?>
</div>


      <!-- Modal -->
          <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel"> Nueva Editorial
                  </h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
      <div class="container">
          <form  class="text-dark"action="<?php echo site_url('editoriales/guardarEditorial') ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_editorial">
              <div class="mb-3 text-dark">
                  <label for="nombre" class="form-label text-dark"><b>Nombre:</b></label>
                  <input id="nombre" type="text" name="nombre" value="" oninput="EspacioLetrasNumeros(this)" placeholder="Ingrese el nombre de la editorial" class="form-control" required>
              </div>
              <div class="mb-3">
                  <label for="telefono" class="form-label"><b>Teléfono:</b></label>
                  <input id="telefono" type="text" name="telefono" value="" oninput="validarNumeros(this)" placeholder="Ingrese el teléfono" class="form-control" required>
              </div>
              <div class="mb-3">
                  <label for="correo" class="form-label"><b>Correo:</b></label>
                  <input id="correo" type="email" name="correo" value="" oninput="" placeholder="Ingrese el correo" class="form-control" required>
              </div>
              <div class="row justify-content-end">
                  <div class="col-auto">
                      <button type="submit" name="button" class="btn btn-success">
                          <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbsp;Guardar&nbsp;
                      </button>
                  </div>
                  <div class="col-auto">
                      <a class="btn btn-danger" href="<?php echo site_url('editoriales/index') ?>">
                          <i class="fa-solid fa-xmark fa-spin"></i>&nbsp;Cancelar&nbsp;
                      </a>
                  </div>
              </div>
          </form>
      </div>
  </div>

  </div>
  </div>
    </div>
<script>
function eliminarRegistro(url) {
              Swal.fire({
                  title: '¿Estas seguro de eliminar este registro?',
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: '¡Sí, eliminalo!',
                  cancelButtonText: 'Cancelar'
              }).then((result) => {
                  if (result.isConfirmed) {
                      // Si el usuario confirma la eliminación, redireccionamos a la URL especificada
                      window.location.href = url;
                  } else {
                      // Si el usuario cancela, mostramos un mensaje de cancelación
                      Swal.fire(
                          'Cancelado',
                          'Tu registro no ha sido eliminado :P',
                          'error'
                      );
                  }
              });
          }
      </script>

      <script type="text/javascript">
      function validarLetras(input) {
        input.value = input.value.replace(/\s+/g, ' ').replace(/[^a-zA-ZñÑ\s]/g, '');
        input.value = input.value.toUpperCase();

      }


      function validarNumeros(input) {
      input.value = input.value.replace(/\D/g, '');
      }

      </script>
      <script type="text/javascript">
      $(document).ready(function() {
          $("#frm_nuevo_editorial").validate({
              rules: {

                  "nombre": {
                      required: true,
                      minlength: 5

                  },
                  "telefono": {
                    required: true,
                    minlength: 7, // Puedes ajustar la longitud según tus necesidades
                    maxlength: 10,
                    pattern: /^\+?([0-9]{1,3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/

                  },

                  "correo": {
                    required: true,
                    email: true

                  }


              },
              messages: {

                  "nombre": {
                      required: "Debe ingresar el nombre de la editorial",
                      minlength: "El nombre no debe tener menos de 5 caracteres"

                  },
                  "telefono": {
                    required: "Por favor, ingresa tu número de teléfono",
                     minlength: "El número de teléfono debe tener al menos 7 dígitos",
                     maxlength: "El número de teléfono no puede tener más de 10 dígitos",
                     pattern: "Por favor, ingresa un número de teléfono válido"
                  },

                  "correo": {
                    required: "Por favor, ingresa tu correo electrónico",
                    email: "Por favor, ingresa un correo electrónico válido"
                  }




              }
          });
      });


      </script>
