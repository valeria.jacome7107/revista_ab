<!-- Footer Start -->
<div class="container-fluid footer py-5 wow fadeIn" data-wow-delay="0.2s">
    <div class="container py-5">
        <div class="row g-5">
            <div class="col-md-6 col-lg-6 col-xl-3">
                <div class="footer-item d-flex flex-column">
                    <h4 class="text-white mb-4"><img src="<?php echo base_url('assets/img/logo.png'); ?>" alt="Logo" width="60px"></i>Revista Victec</h4>
                    <p>Transforma la realidad existente a través de la obtención de un conocimiento práctico con una metodología diferente.
                    </p>
                    <div class="d-flex align-items-center">
                        <i class="fas fa-share fa-2x text-white me-2"></i>
                        <a class="btn-square btn btn-primary text-white rounded-circle mx-1" href=""><i class="fab fa-facebook-f"></i></a>
                        <a class="btn-square btn btn-primary text-white rounded-circle mx-1" href=""><i class="fab fa-twitter"></i></a>
                        <a class="btn-square btn btn-primary text-white rounded-circle mx-1" href=""><i class="fab fa-instagram"></i></a>
                        <a class="btn-square btn btn-primary text-white rounded-circle mx-1" href=""><i class="fab fa-linkedin-in"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-3">
                <div class="footer-item d-flex flex-column">
                    <h4 class="mb-4 text-white">Enlaces rápidos</h4>
                    <a href=""><i class="fas fa-angle-right me-2"></i> Sobre la revista</a>
                    <a href=""><i class="fas fa-angle-right me-2"></i> Editorial</a>
                    <a href=""><i class="fas fa-angle-right me-2"></i> Revista</a>
                    <a href=""><i class="fas fa-angle-right me-2"></i> Artículos Científicos</a>
                    <a href=""><i class="fas fa-angle-right me-2"></i> Autores</a>
                    <a href=""><i class="fas fa-angle-right me-2"></i> Nuestro Equipo</a>
                </div>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-3">
                <div class="footer-item d-flex flex-column">
                    <h4 class="mb-4 text-white">Información de Contacto</h4>
                    <a href=""><i class="fa fa-map-marker-alt me-2"></i>Latacunga</a>
                    <a href=""><i class="fas fa-envelope me-2"></i>correo1@example.com</a>
                    <a href=""><i class="fas fa-envelope me-2"></i>correo2@example.com</a>
                    <a href=""><i class="fas fa-phone me-2"></i>095623145</a>
                    <a href="" class="mb-3"><i class="fas fa-print me-2"></i>02598761</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Footer End -->

<!-- Copyright Start -->
<div class="container-fluid copyright py-4">
    <div class="container">
        <div class="row g-4 align-items-center">
            <div class="col-md-6 text-center text-md-start mb-md-0">
                <span class="text-white"><a href="#"><i class="fas fa-copyright text-light me-2"></i>Victec</a>, Todos los derechos reservados</span>
            </div>
            <div class="col-md-6 text-center text-md-end text-white">
                <!--/*** This template is libre as long as you keep the below author’s credit link/attribution link/backlink. ***/-->
                <!--/*** If you'd like to use the template without the below author’s credit link/attribution link/backlink, ***/-->
                <!--/*** you can purchase the Credit Renovar la licencia from "https://htmlcodex.com/credit-removal". ***/-->
                Designed By <a class="border-bottom" href="https://htmlcodex.com">HTML Codex</a> Distrribuido por<a class="border-bottom" href="https://themewagon.com">Integrantes</a>
            </div>
        </div>
    </div>
</div>
<!-- Copyright End -->

<!-- Back to Top -->
<a href="#" class="btn btn-primary btn-lg-square back-to-top"><i class="fa fa-arrow-up"></i></a>


<!-- JavaScript Libraries -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.20.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url('assets/lib/wow/wow.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/lib/easing/easing.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/lib/waypoints/waypoints.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/lib/owlcarousel/owl.carousel.min.js'); ?>"></script>

<!-- Select2 JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-beta.1/js/select2.min.js"></script>
<!-- Template Javascript -->
<script src="<?php echo base_url('assets/js/main.js'); ?>"></script>


</body>

</html>
