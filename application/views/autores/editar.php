<div style="padding: 150px 70px 20px 100px">

    <h1>
        <b>
            <i class="fa-solid fa-pen-to-square"></i>
            EDITAR AUTOR
        </b>
    </h1>
    <br>

    <form class="text-dark" action="<?php echo site_url('autores/actualizarAutor'); ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_autor">

        <input type="hidden" value="<?php echo $autorEditar->id; ?>" name="id" id="id">

        <div class="mb-3 text-dark">
            <label for="nombre" class="form-label text-dark"><b>Nombre:</b></label>
            <input id="nombre" type="text" name="nombre" value="<?php echo $autorEditar->nombre; ?>" oninput="EspacioLetrasNumeros(this)" placeholder="Ingrese el nombre del autor" class="form-control" required>
        </div>
        <div class="mb-3 text-dark">
            <label for="apellido" class="form-label text-dark"><b>Apellido:</b></label>
            <input id="apellido" type="text" name="apellido" value="<?php echo $autorEditar->apellido; ?>" oninput="EspacioLetrasNumeros(this)" placeholder="Ingrese el apellido del autor" class="form-control" required>
        </div>
        <div class="mb-3 text-dark">
            <label for="direccion" class="form-label text-dark"><b>Dirección:</b></label>
            <input id="direccion" type="text" name="direccion" value="<?php echo $autorEditar->direccion; ?>" oninput="EspacioLetrasNumeros(this)" placeholder="Ingrese la dirección" class="form-control" required>
        </div>
        <div class="mb-3 text-dark">
            <label for="pais" class="form-label text-dark"><b>País:</b></label>
            <input id="pais" type="text" name="pais" value="<?php echo $autorEditar->pais; ?>" oninput="EspacioLetrasNumeros(this)" placeholder="Ingrese el país" class="form-control" required>
        </div>
        <div class="mb-3">
            <label for="correo" class="form-label"><b>Correo:</b></label>
            <input id="correo" type="email" name="correo" value="<?php echo $autorEditar->correo; ?>" oninput="" placeholder="Ingrese el correo" class="form-control" required>
        </div>
        <div class="mb-3">
            <label for="telefono" class="form-label"><b>Teléfono:</b></label>
            <input id="telefono" type="text" name="telefono" value="<?php echo $autorEditar->telefono; ?>" oninput="validarNumeros(this)" placeholder="Ingrese el teléfono" class="form-control" required>
        </div>

        <br>
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-warning"> <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbspActualizar&nbsp</button>
                &nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a class="btn btn-danger" href=" <?php echo site_url('autores/index') ?> "><i class="fa-solid fa-xmark  fa-spin"></i>&nbspCancelar&nbsp</a>
            </div>

        </div>

    </form>

</div>
<script type="text/javascript">
function validarLetras(input) {
  input.value = input.value.replace(/\s+/g, ' ').replace(/[^a-zA-ZñÑ\s]/g, '');
  input.value = input.value.toUpperCase();

}


function validarNumeros(input) {
input.value = input.value.replace(/\D/g, '');
}

</script>
<script type="text/javascript">
$(document).ready(function() {
    $("#frm_nuevo_autor").validate({
        rules: {

            "nombre": {
                required: true,
                minlength: 5

            },

            "apellido": {
              required: true,
              minlength: 5
            },

            "direccion": {
                required: true,
                minlength: 5,
                maxlength: 150

            },

            "pais": {
                required: true,
                minlength: 5,
                maxlength: 150

            },

            "correo": {
              required: true,
              email: true

            },

            "telefono": {
              required: true,
              minlength: 7, // Puedes ajustar la longitud según tus necesidades
              maxlength: 10,
              pattern: /^\d+$/

            }
        },
        messages: {

            "nombre": {
                required: "Debe ingresar el nombre del autor",
                minlength: "El nombre no debe tener menos de 5 caracteres"

            },
            "apellido": {
              required: "Debe apellido el nombre del autor",
              minlength: "El apellido no debe tener menos de 5 caracteres"

            },

            "direccion": {
                required: "Debe ingresar la direccion del autor",
                minlength: "Debe ingresar 5 o más caracteres",
                maxlength: "solo se permite hasta 150 caracteres"

            },

            "pais": {
              required: "Debe ingresar el pais de residencia",
              minlength: "Debe ingresar 5 o más caracteres",
              maxlength: "solo se permite hasta 150 caracteres"
            },

            "correo": {
              required: "Por favor, ingresa tu correo electrónico",
              email: "Por favor, ingresa un correo electrónico válido"
            },


            "telefono": {
              required: "Por favor, ingresa tu número de teléfono",
               minlength: "El número de teléfono debe tener al menos 7 dígitos",
               maxlength: "El número de teléfono no puede tener más de 10 dígitos",
               pattern: "Por favor, ingresa un número de teléfono válido"
            }

        }
    });
});


</script>
