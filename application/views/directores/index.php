<div style="padding: 150px 70px 20px 100px">
     <div class="text-center">
         <h1><i class="fa-solid fa-book"></i>&nbsp;&nbsp;Directores</h1>
    </div>
    <div class="row">
    <div class="col-md-12 text-end">

      <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
         <i class="fa fa-plus-circle fa-1x"></i> Agregar nuevo director
      </button>


    </div>

  </div><br>


  <?php if ($listadoDirectores): ?>
    <table class="table table-striped text-center">
    <thead class="table-dark">
        <tr>
            <th>ID</th>
            <th>CÉDULA</th>
            <th>NOMBRE</th>
            <th>APELLIDO</th>
            <th>TELÉFONO</th>
            <th>CORREO</th>
            <th>FIRMA</th>
            <th>EDITORIAL</th>
            <th>ACCIONES</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($listadoDirectores as $director): ?>
            <tr>
                <td class="text-dark"><?php echo $director->id; ?></td>
                <td class="text-dark"><?php echo $director->cedula; ?></td>
                <td class="text-dark"><?php echo $director->nombre; ?></td>
                <td class="text-dark"><?php echo $director->apellido; ?></td>
                <td class="text-dark"><?php echo $director->telefono; ?></td>
                <td class="text-dark"><?php echo $director->correo; ?></td>
                <td>
                      <?php if ($director->fotografia!=""): ?>
                        <img src="<?php echo base_url('uploads/directores/').$director->fotografia; ?>"
                        height="75px" alt="">
                      <?php else: ?>
                        N/A
                      <?php endif; ?>
                    </td>
                <td class="text-dark"><?php echo $director->nombre_editorial; ?></td>
                <td>
                    <a href="<?php echo site_url('directores/editar/').$director->id; ?>" class="btn btn-warning" title="Editar">
                        <i class="fa fa-pen"></i>
                    </a>
                    <a href="#" class="btn btn-danger" onclick="eliminarRegistro('<?php echo site_url('directores/borrar/').$director->id; ?>')">
                        <i class="fa fa-trash"></i>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>


  <div class="modal-footer">

  </div>

              </div>
            </div>
          </div>
    <?php else: ?>
          <div class="alert alert-danger">
              No se encontro editoriales registrados
          </div>
  <?php endif; ?>
</div>


      <!-- Modal -->
          <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel"> Nuevo Director
                  </h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
      <div class="container">
          <form  class="text-dark" action="<?php echo site_url('directores/guardarDirector') ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_director">
            <div class="mb-3 text-dark">
              <label for="fkid_edi"><b>Editorial:</b></label>
                  <select name="fkid_edi" id="fkid_edi" class="form-control" required>
                      <option value="">--Seleccione la Editorial--</option>
                      <?php foreach ($listadoEditoriales as $editorial): ?>
                          <option value="<?php echo $editorial->id; ?>"><?php echo $editorial->nombre; ?></option>
                      <?php endforeach; ?>
                  </select>
            </div>

            <div class="mb-3 text-dark">
                <label for="cedula" class="form-label text-dark"><b>Cédula:</b></label>
                <input id="cedula" type="text" name="cedula" value="" oninput="validarNumeros(this)" placeholder="Ingrese al cédula del director" class="form-control" required>
            </div>
              <div class="mb-3 text-dark">
                  <label for="nombre" class="form-label text-dark"><b>Nombre:</b></label>
                  <input id="nombre" type="text" name="nombre" value="" oninput="EspacioLetrasNumeros(this)" placeholder="Ingrese el nombre director" class="form-control" required>
              </div>
              <div class="mb-3 text-dark">
                  <label for="apellido" class="form-label text-dark"><b>Apellido:</b></label>
                  <input id="apellido" type="text" name="apellido" value="" oninput="EspacioLetrasNumeros(this)" placeholder="Ingrese el apellido director" class="form-control" required>
              </div>
              <div class="mb-3">
                  <label for="telefono" class="form-label"><b>Teléfono:</b></label>
                  <input id="telefono" type="text" name="telefono" value="" oninput="validarNumeros(this)" placeholder="Ingrese el teléfono" class="form-control" required>
              </div>
              <div class="mb-3">
                  <label for="correo" class="form-label"><b>Correo:</b></label>
                  <input id="correo" type="email" name="correo" value="" oninput="" placeholder="Ingrese el correo" class="form-control" required>
              </div>
              <div class="mb-3">
                <label for="fotografia"> <b>Firma: </b> </label>
                <input type="file" name="fotografia" id="fotografia" value="" class="form-control" required accept="image/*"><br>
              </div>
              <div class="row justify-content-end">
                  <div class="col-auto">
                      <button type="submit" name="button" class="btn btn-success">
                          <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbsp;Guardar&nbsp;
                      </button>
                  </div>
                  <div class="col-auto">
                      <a class="btn btn-danger" href="<?php echo site_url('directores/index') ?>">
                          <i class="fa-solid fa-xmark fa-spin"></i>&nbsp;Cancelar&nbsp;
                      </a>
                  </div>
              </div>
          </form>
      </div>
  </div>

  </div>
  </div>
    </div>
<script>
function eliminarRegistro(url) {
              Swal.fire({
                  title: '¿Estas seguro de eliminar este registro?',
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: '¡Sí, eliminalo!',
                  cancelButtonText: 'Cancelar'
              }).then((result) => {
                  if (result.isConfirmed) {
                      // Si el usuario confirma la eliminación, redireccionamos a la URL especificada
                      window.location.href = url;
                  } else {
                      // Si el usuario cancela, mostramos un mensaje de cancelación
                      Swal.fire(
                          'Cancelado',
                          'Tu registro no ha sido eliminado :P',
                          'error'
                      );
                  }
              });
          }
      </script>

      <script type="text/javascript">
      function validarLetras(input) {
        input.value = input.value.replace(/\s+/g, ' ').replace(/[^a-zA-ZñÑ\s]/g, '');
        input.value = input.value.toUpperCase();

      }


      function validarNumeros(input) {
      input.value = input.value.replace(/\D/g, '');
      }

      </script>
      <script>
            $(document).ready(function() {
              $('#fkid_edi').select2({
                placeholder: "--Seleccione la Editorial--", // Texto del placeholder
                allowClear: true // Opcional, para permitir que se limpie la selección
              });
            });
            </script>
      <script>
        $(document).ready(function () {
          $("#fotografia_nueva").fileinput({
            //showUpload:false
            //showRemove: false,
            language:'es',
          });
        });
      </script>

      <script type="text/javascript">
      $(document).ready(function() {
          $("#frm_nuevo_director").validate({
              rules: {
                "fkid_edi": {
                    required: true

                },
                "cedula": {
                    required: true,
                    minlength: 10

                },
                  "nombre": {
                      required: true,
                      minlength: 5

                  },

                  "apellido": {
                    required: true,
                    minlength: 5
                  },
                  "telefono": {
                    required: true,
                    minlength: 7, // Puedes ajustar la longitud según tus necesidades
                    maxlength: 10,
                    pattern: /^\d+$/

                  },

                  "correo": {
                    required: true,
                    email: true

                  }
              },
              messages: {
                "fkid_edi": {
                    required: "Escoja la editorial"

                },
                "cedula": {
                    required: "Ingrese el número de cédula",
                    minlength: "La cédula no debe tener menos de 10 numeros"

                },
                  "nombre": {
                      required: "Debe ingresar el nombre del autor",
                      minlength: "El nombre no debe tener menos de 5 caracteres"

                  },
                  "apellido": {
                    required: "Debe ingresar el apellido del autor",
                    minlength: "El apellido no debe tener menos de 5 caracteres"

                  },
                  "telefono": {
                    required: "Por favor, ingresa tu número de teléfono",
                     minlength: "El número de teléfono debe tener al menos 7 dígitos",
                     maxlength: "El número de teléfono no puede tener más de 10 dígitos",
                     pattern: "Por favor, ingresa un número de teléfono válido"
                  },

                  "correo": {
                    required: "Por favor, ingresa tu correo electrónico",
                    email: "Por favor, ingresa un correo electrónico válido"
                  }




              }
          });
      });


      </script>
