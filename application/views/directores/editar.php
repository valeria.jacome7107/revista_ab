<div style="padding: 150px 100px 20px 100px">
<h1>
<b>
  <i class="fa fa-plus-circle"></i>
  EDITAR DIRECTOR
</b>
</h1>
<br>

<form class="" action="<?php echo site_url('directores/actualizarDirector'); ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_director">
  <div class="mb-3 text-dark">
    <label for="fkid_edi"><b>Editorial:</b></label>
  <select name="fkid_edi" id="fkid_edi" class="form-control selectpicker" data-live-search="true" data-live-search-style="startsWith"  required>
      <option value="">--Seleccione la editorial--</option>
      <?php foreach ($listadoEditoriales as $editorial): ?>
          <?php if ($editorial->id == $directorEditar->fkid_edi): ?>
              <option value="<?php echo $editorial->id; ?>" selected><?php echo $editorial->nombre; ?></option>
          <?php else: ?>
              <option value="<?php echo $editorial->id; ?>"><?php echo $editorial->nombre; ?></option>
          <?php endif; ?>
      <?php endforeach; ?>
  </select>
  </div>
	<input type="hidden" value="<?php echo $directorEditar->id; ?>" name="id" id="id">
  <div class="mb-3 text-dark">
      <label for="cedula" class="form-label text-dark"><b>Cédula:</b></label>
      <input id="cedula" type="text" name="cedula" value="<?php echo $directorEditar->cedula; ?>" oninput="validarNumeros(this)" placeholder="Ingrese al cédula del director" class="form-control" required>
  </div>
    <div class="mb-3 text-dark">
        <label for="nombre" class="form-label text-dark"><b>Nombre:</b></label>
        <input id="nombre" type="text" name="nombre" value="<?php echo $directorEditar->nombre; ?>" oninput="EspacioLetrasNumeros(this)" placeholder="Ingrese el nombre director" class="form-control" required>
    </div>
    <div class="mb-3 text-dark">
        <label for="apellido" class="form-label text-dark"><b>Apellido:</b></label>
        <input id="apellido" type="text" name="apellido" value="<?php echo $directorEditar->apellido; ?>" oninput="EspacioLetrasNumeros(this)" placeholder="Ingrese el apellido director" class="form-control" required>
    </div>
    <div class="mb-3">
        <label for="telefono" class="form-label"><b>Teléfono:</b></label>
        <input id="telefono" type="text" name="telefono" value="<?php echo $directorEditar->telefono; ?>" oninput="validarNumeros(this)" placeholder="Ingrese el teléfono" class="form-control" required>
    </div>
    <div class="mb-3">
        <label for="correo" class="form-label"><b>Correo:</b></label>
        <input id="correo" type="email" name="correo" value="<?php echo $directorEditar->correo; ?>" oninput="" placeholder="Ingrese el correo" class="form-control" required>
    </div>
    <div class="mb-3">
      <label for=""><b>Firma actual del director:</b></label><br>
        <?php if (!empty($directorEditar->fotografia)) : ?>
            <img src="<?php echo base_url('uploads/directores/' . $directorEditar->fotografia); ?>" alt="Firma del director" width="200"><br>
        <?php else: ?>
            <p>No hay firma disponible</p>
        <?php endif; ?>
        <br>
         <label for=""><b>Firma nueva:</b></label><br>

         <input type="file" name="fotografia_nueva" id="fotografia_nueva" accept="image/*" class="form-control">

    </div>

<br>
<div class="row">
  <div class="col-md-12 text-center">
    <button type="submit" name="button" class="btn btn-warning"> <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbspActualizar&nbsp</button>
    &nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a class="btn btn-danger" href=" <?php echo site_url('directores/index') ?> "><i class="fa-solid fa-xmark  fa-spin"></i>&nbspCancelar&nbsp</a>
  </div>

</div>

</form>

</div>
<script type="text/javascript">
function validarLetras(input) {
  input.value = input.value.replace(/\s+/g, ' ').replace(/[^a-zA-ZñÑ\s]/g, '');
  input.value = input.value.toUpperCase();

}


function validarNumeros(input) {
input.value = input.value.replace(/\D/g, '');
}

</script>
<script>
      $(document).ready(function() {
        $('#fkid_edi').select2({
          placeholder: "--Seleccione la Editorial--", // Texto del placeholder
          allowClear: true // Opcional, para permitir que se limpie la selección
        });
      });
      </script>
<script>
  $(document).ready(function () {
    $("#fotografia_nueva").fileinput({
      //showUpload:false
      //showRemove: false,
      language:'es',
    });
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
    $("#frm_nuevo_director").validate({
        rules: {
          "fkid_edi": {
              required: true

          },
          "cedula": {
              required: true,
              minlength: 10

          },
            "nombre": {
                required: true,
                minlength: 5

            },

            "apellido": {
              required: true,
              minlength: 5
            },
            "telefono": {
              required: true,
              minlength: 7, // Puedes ajustar la longitud según tus necesidades
              maxlength: 10,
              pattern: /^\d+$/

            },

            "correo": {
              required: true,
              email: true

            }
        },
        messages: {
          "fkid_edi": {
              required: "Escoja la editorial"

          },
          "cedula": {
              required: "Ingrese el número de cédula",
              minlength: "La cédula no debe tener menos de 10 numeros"

          },
            "nombre": {
                required: "Debe ingresar el nombre del autor",
                minlength: "El nombre no debe tener menos de 5 caracteres"

            },
            "apellido": {
              required: "Debe apellido el nombre del autor",
              minlength: "El apellido no debe tener menos de 5 caracteres"

            },
            "telefono": {
              required: "Por favor, ingresa tu número de teléfono",
               minlength: "El número de teléfono debe tener al menos 7 dígitos",
               maxlength: "El número de teléfono no puede tener más de 10 dígitos",
               pattern: "Por favor, ingresa un número de teléfono válido"
            },

            "correo": {
              required: "Por favor, ingresa tu correo electrónico",
              email: "Por favor, ingresa un correo electrónico válido"
            }




        }
    });
});


</script>
