
<div style="padding: 150px 70px 20px 100px">
     <div class="text-center">
       <h1><i class="fa-solid fa-book-open"></i>&nbsp;&nbsp;Autorias</h1>
    </div>
    <div class="row">
    <div class="col-md-12 text-end">

      <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
         <i class="fa fa-plus-circle fa-1x"></i> Agregar Autoria
      </button>


    </div>

  </div><br>


  <?php if ($listadoAutorias): ?>
    <table class="table table-striped text-center">
    <thead class="table-dark">
        <tr>
            <th>ID</th>
            <th>ARTÍCULO</th>
            <th>AUTOR</th>
            <th>ACCIONES</th>

        </tr>
    </thead>
    <tbody>
        <?php foreach ($listadoAutorias as $autoria): ?>
            <tr>
                <td class="text-dark"><?php echo $autoria->id; ?></td>
                <td class="text-dark"><?php echo $autoria->nombre_articulo; ?></td>
                <td class="text-dark"><?php echo $autoria->nombre_autor; ?></td>

                <td>
                    <a href="<?php echo site_url('autorias/editar/').$autoria->id; ?>" class="btn btn-warning" title="Editar">
                        <i class="fa fa-pen"></i>
                    </a>
                    <a href="#" class="btn btn-danger" onclick="eliminarRegistro('<?php echo site_url('autorias/borrar/').$articulo->id; ?>')">
                        <i class="fa fa-trash"></i>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>



  <div class="modal-footer">

  </div>

              </div>
            </div>
          </div>
    <?php else: ?>
          <div class="alert alert-danger">
              No se encontro autorias registradas
          </div>
  <?php endif; ?>
</div>

<!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"> Nueva Autoria
            </h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
<div class="container">
    <form  class="text-dark"action="<?php echo site_url('autorias/guardarAutoria') ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_corresponsal">

        <div class="mb-3 text-dark">
      <label for="fkid_autor"><b>Autor:</b></label>
      <select name="fkid_autor" id="fkid_autor" class="form-control" required>
          <option value="">--Seleccione el autor--</option>
          <?php foreach ($listadoAutores as $autor): ?>
              <option value="<?php echo $autor->id; ?>"><?php echo $autor->nombre; ?> <?php echo $autor->apellido; ?></option>
          <?php endforeach; ?>
      </select>
        </div>

        <div class="mb-3 text-dark">
      <label for="fkid_autor"><b>Artículo:</b></label>
      <select name="fkid_arti" id="fkid_arti" class="form-control" required>
          <option value="">--Seleccione el artículo--</option>
          <?php foreach ($listadoArticulos as $articulo): ?>
              <option value="<?php echo $articulo->id; ?>"><?php echo $articulo->nombre; ?></option>
          <?php endforeach; ?>
      </select>
        </div>




        <div class="row justify-content-end">
            <div class="col-auto">
                <button type="submit" name="button" class="btn btn-success">
                    <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbsp;Guardar&nbsp;
                </button>
            </div>
            <div class="col-auto">
                <a class="btn btn-danger" href="<?php echo site_url('autoria/index') ?>">
                    <i class="fa-solid fa-xmark fa-spin"></i>&nbsp;Cancelar&nbsp;
                </a>
            </div>
        </div>
    </form>
</div>
</div>

</div>

</div>
</div>
<script>
function eliminarRegistro(url) {
              Swal.fire({
                  title: '¿Estas seguro de eliminar este registro?',
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: '¡Sí, eliminalo!',
                  cancelButtonText: 'Cancelar'
              }).then((result) => {
                  if (result.isConfirmed) {
                      // Si el usuario confirma la eliminación, redireccionamos a la URL especificada
                      window.location.href = url;
                  } else {
                      // Si el usuario cancela, mostramos un mensaje de cancelación
                      Swal.fire(
                          'Cancelado',
                          'Tu registro no ha sido eliminado :P',
                          'error'
                      );
                  }
              });
          }
      </script>
