<div style="padding: 150px 70px 20px 100px">

    <h1>
        <b>
            <i class="fa-solid fa-pen-to-square"></i>
            EDITAR AUTORIA
        </b>
    </h1>
    <br>

    <form class="text-dark" action="<?php echo site_url('autorias/actualizarAutoria'); ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_revista">

        <input type="hidden" value="<?php echo $autoriaEditar->id; ?>" name="id" id="id">


        <label for="fkid_arti"><b>Articulo:</b></label>
      <select name="fkid_arti" id="fkid_arti" class="form-control selectpicker" data-live-search="true" data-live-search-style="startsWith"  required>
          <option value="">--Seleccione el articulo--</option>
          <?php foreach ($listadoArticulos as $articulo): ?>
              <?php if ($articulo->id == $autoriaEditar->fkid_arti): ?>
                  <option value="<?php echo $articulo->id; ?>" selected><?php echo $articulo->nombre; ?></option>
              <?php else: ?>
                  <option value="<?php echo $articulo->id; ?>"><?php echo $articulo->nombre; ?></option>
              <?php endif; ?>
          <?php endforeach; ?>
      </select>
      <br>

      <label for="fkid_autor"><b>Autor:</b></label>
    <select name="fkid_autor" id="fkid_autor" class="form-control selectpicker" data-live-search="true" data-live-search-style="startsWith"  required>
        <option value="">--Seleccione el autor--</option>
        <?php foreach ($listadoAutores as $autor): ?>
            <?php if ($autor->id == $autoriaEditar->fkid_autor): ?>
                <option value="<?php echo $autor->id; ?>" selected><?php echo $autor->nombre; ?></option>
            <?php else: ?>
                <option value="<?php echo $autor->id; ?>"><?php echo $autor->nombre; ?></option>
            <?php endif; ?>
        <?php endforeach; ?>
    </select>
    <br>


        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-warning"> <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbspActualizar&nbsp</button>
                &nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a class="btn btn-danger" href=" <?php echo site_url('autorias/index') ?> "><i class="fa-solid fa-xmark  fa-spin"></i>&nbspCancelar&nbsp</a>
            </div>

        </div>

    </form>

</div>
