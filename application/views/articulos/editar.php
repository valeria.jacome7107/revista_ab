<div style="padding: 150px 70px 20px 100px">

    <h1>
        <b>
            <i class="fa-solid fa-pen-to-square"></i>
            EDITAR ARTICULO
        </b>
    </h1>
    <br>

    <form class="text-dark" action="<?php echo site_url('articulos/actualizarArticulo'); ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_articulo">

        <input type="hidden" value="<?php echo $articuloEditar->id; ?>" name="id" id="id">


        <label for="fkid_revis"><b>Revista:</b></label>
      <select name="fkid_revis" id="fkid_revis" class="form-control selectpicker" data-live-search="true" data-live-search-style="startsWith"  required>
          <option value="">--Seleccione la revista--</option>
          <?php foreach ($listadoRevistas as $revista): ?>
              <?php if ($revista->id == $articuloEditar->fkid_revis): ?>
                  <option value="<?php echo $revista->id; ?>" selected><?php echo $revista->nombre; ?></option>
              <?php else: ?>
                  <option value="<?php echo $revista->id; ?>"><?php echo $revista->nombre; ?></option>
              <?php endif; ?>
          <?php endforeach; ?>
      </select>
      <br>


      <div class="mb-3 text-dark">
          <label for="nombre" class="form-label text-dark"><b>Nombre:</b></label>
          <input id="nombre" type="text" name="nombre" value="<?php echo $articuloEditar->nombre; ?>" oninput="EspacioLetrasNumeros(this)" placeholder="Ingrese el nombre de la revista" class="form-control" required>
      </div>
      <div class="mb-3">
          <label for="forma" class="form-label"><b>Forma:</b></label>
          <input id="forma" type="text" name="forma" value="<?php echo $articuloEditar->forma; ?>"  placeholder="" class="form-control" required>
      </div>
      <div class="mb-3">
          <label for="formato" class="form-label"><b>Formato:</b></label>
          <input id="formato" type="text" name="formato" value="<?php echo $articuloEditar->formato; ?>"  placeholder="Ingrese el formato" class="form-control" required>
      </div>

      <div class="mb-3">
          <label for="direccion_electronica" class="form-label"><b>Dirección electrónica:</b></label>
          <input id="direccion_electronica" type="text" name="direccion_electronica" value="<?php echo $articuloEditar->direccion_electronica; ?>"  placeholder="Ingrese la dirección electrónica" class="form-control" required>
      </div>

      <div class="mb-3">
          <label for="num_paginas" class="form-label"><b>Número de páginas:</b></label>
          <input id="num_paginas" type="text" name="num_paginas" value="<?php echo $articuloEditar->num_paginas; ?>"  placeholder="Ingrese el numero de la revista" class="form-control" required>
      </div>


        <br>
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-warning"> <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbspActualizar&nbsp</button>
                &nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a class="btn btn-danger" href=" <?php echo site_url('articulos/index') ?> "><i class="fa-solid fa-xmark  fa-spin"></i>&nbspCancelar&nbsp</a>
            </div>

        </div>

    </form>

</div>
