
<div style="padding: 150px 70px 20px 100px">
     <div class="text-center">
       <h1><i class="fa-solid fa-book-open"></i>&nbsp;&nbsp;Articulos</h1>
    </div>
    <div class="row">
    <div class="col-md-12 text-end">

      <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
         <i class="fa fa-plus-circle fa-1x"></i> Agregar Articulo
      </button>


    </div>

  </div><br>


  <?php if ($listadoArticulos): ?>
    <table class="table table-striped text-center">
    <thead class="table-dark">
        <tr>
            <th>ID</th>
            <th>NOMBRE</th>
            <th>FORMA</th>
            <th>FORMATO</th>
            <th>DIRECCION ELECTRONICA</th>
            <th>NUM_PAGINAS</th>
            <th>REVISTA</th>
            <th>ACCIONES</th>

        </tr>
    </thead>
    <tbody>
        <?php foreach ($listadoArticulos as $articulo): ?>
            <tr>
                <td class="text-dark"><?php echo $articulo->id; ?></td>
                <td class="text-dark"><?php echo $articulo->nombre; ?></td>
                <td class="text-dark"><?php echo $articulo->forma; ?></td>
                <td class="text-dark"><?php echo $articulo->formato; ?></td>
                <td class="text-dark"><?php echo $articulo->direccion_electronica; ?></td>
                <td class="text-dark"><?php echo $articulo->num_paginas; ?></td>
                <td class="text-dark"><?php echo $articulo->nombre_revista; ?></td>


                <td>
                    <a href="<?php echo site_url('articulos/editar/').$articulo->id; ?>" class="btn btn-warning" title="Editar">
                        <i class="fa fa-pen"></i>
                    </a>
                    <a href="#" class="btn btn-danger" onclick="eliminarRegistro('<?php echo site_url('articulos/borrar/').$articulo->id; ?>')">
                        <i class="fa fa-trash"></i>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>



  <div class="modal-footer">

  </div>

              </div>
            </div>
          </div>
    <?php else: ?>
          <div class="alert alert-danger">
              No se encontro revistas registrados
          </div>
  <?php endif; ?>
</div>

<!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"> Nuevo Articulo
            </h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
<div class="container">
    <form  class="text-dark"action="<?php echo site_url('articulos/guardarArticulo') ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_corresponsal">

        <div class="mb-3 text-dark">
      <label for="fkid_revis"><b>Revista:</b></label>
      <select name="fkid_revis" id="fkid_revis" class="form-control" required>
          <option value="">--Seleccione la revista--</option>
          <?php foreach ($listadoRevistas as $revista): ?>
              <option value="<?php echo $revista->id; ?>"><?php echo $revista->nombre; ?></option>
          <?php endforeach; ?>
      </select>
        </div>

        <div class="mb-3 text-dark">
            <label for="nombre" class="form-label text-dark"><b>Nombre:</b></label>
            <input id="nombre" type="text" name="nombre" value="" oninput="EspacioLetrasNumeros(this)" placeholder="Ingrese el nombre de la revista" class="form-control" required>
        </div>
        <div class="mb-3">
            <label for="forma" class="form-label"><b>Forma:</b></label>
            <input id="forma" type="text" name="forma" value=""  placeholder="" class="form-control" required>
        </div>
        <div class="mb-3">
            <label for="formato" class="form-label"><b>Formato:</b></label>
            <input id="formato" type="text" name="formato" value=""  placeholder="Ingrese el formato" class="form-control" required>
        </div>

        <div class="mb-3">
            <label for="direccion_electronica" class="form-label"><b>Dirección electrónica:</b></label>
            <input id="direccion_electronica" type="text" name="direccion_electronica" value=""  placeholder="Ingrese la dirección electrónica" class="form-control" required>
        </div>

        <div class="mb-3">
            <label for="num_paginas" class="form-label"><b>Número de páginas:</b></label>
            <input id="num_paginas" type="text" name="num_paginas" value=""  placeholder="Ingrese el numero de la revista" class="form-control" required>
        </div>



        <div class="row justify-content-end">
            <div class="col-auto">
                <button type="submit" name="button" class="btn btn-success">
                    <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbsp;Guardar&nbsp;
                </button>
            </div>
            <div class="col-auto">
                <a class="btn btn-danger" href="<?php echo site_url('revistas/index') ?>">
                    <i class="fa-solid fa-xmark fa-spin"></i>&nbsp;Cancelar&nbsp;
                </a>
            </div>
        </div>
    </form>
</div>
</div>

</div>

</div>
</div>
<script>
function eliminarRegistro(url) {
              Swal.fire({
                  title: '¿Estas seguro de eliminar este registro?',
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: '¡Sí, eliminalo!',
                  cancelButtonText: 'Cancelar'
              }).then((result) => {
                  if (result.isConfirmed) {
                      // Si el usuario confirma la eliminación, redireccionamos a la URL especificada
                      window.location.href = url;
                  } else {
                      // Si el usuario cancela, mostramos un mensaje de cancelación
                      Swal.fire(
                          'Cancelado',
                          'Tu registro no ha sido eliminado :P',
                          'error'
                      );
                  }
              });
          }
      </script>
