
<div style="padding: 150px 70px 20px 100px">
     <div class="text-center">
       <h1><i class="fa-solid fa-book-open"></i>&nbsp;&nbsp;Revistas</h1>
    </div>
    <div class="row">
    <div class="col-md-12 text-end">

      <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
         <i class="fa fa-plus-circle fa-1x"></i> Agregar Revista
      </button>


    </div>

  </div><br>


  <?php if ($listadoRevistas): ?>
    <table class="table table-striped text-center">
    <thead class="table-dark">
        <tr>
            <th>ID</th>
            <th>NOMBRE</th>
            <th>FECHA PUBLICACIÓN</th>
            <th>ISSN</th>
            <th>VOLUMEN</th>
            <th>NÚMERO</th>
            <th>EDITORIAL</th>
            <th>LOGO</th>
            <th>ACCIONES</th>

        </tr>
    </thead>
    <tbody>
        <?php foreach ($listadoRevistas as $revista): ?>
            <tr>
                <td class="text-dark"><?php echo $revista->id; ?></td>
                <td class="text-dark"><?php echo $revista->nombre; ?></td>
                <td class="text-dark"><?php echo $revista->fecha_publicacion; ?></td>
                <td class="text-dark"><?php echo $revista->issn; ?></td>
                <td class="text-dark"><?php echo $revista->volumen; ?></td>
                <td class="text-dark"><?php echo $revista->numero; ?></td>
                <td class="text-dark"><?php echo $revista->nombre_editorial; ?></td>


                <td>
                    <?php if ($revista->logo!=""): ?>
                    <img src="<?php echo base_url('uploads/revistas/').$revista->logo;?>"
                    height="100px" alt="">
                    <?php else: ?>N/A<?php endif; ?>

                </td>



                <td>
                    <a href="<?php echo site_url('revistas/editar/').$revista->id; ?>" class="btn btn-warning" title="Editar">
                        <i class="fa fa-pen"></i>
                    </a>
                    <a href="#" class="btn btn-danger" onclick="eliminarRegistro('<?php echo site_url('revistas/borrar/').$revista->id; ?>')">
                        <i class="fa fa-trash"></i>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>



  <div class="modal-footer">

  </div>

              </div>
            </div>
          </div>
    <?php else: ?>
          <div class="alert alert-danger">
              No se encontro revistas registrados
          </div>
  <?php endif; ?>
</div>

<!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"> Nueva Revista
            </h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
<div class="container">
    <form  class="text-dark"action="<?php echo site_url('revistas/guardarRevista') ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_corresponsal">

        <div class="mb-3 text-dark">
      <label for="fkid_edi"><b>Editorial:</b></label>
      <select name="fkid_edi" id="fkid_edi" class="form-control" required>
          <option value="">--Seleccione la editorial--</option>
          <?php foreach ($listadoEditoriales as $editorial): ?>
              <option value="<?php echo $editorial->id; ?>"><?php echo $editorial->nombre; ?></option>
          <?php endforeach; ?>
      </select>
        </div>

        <div class="mb-3 text-dark">
            <label for="nombre" class="form-label text-dark"><b>Nombre:</b></label>
            <input id="nombre" type="text" name="nombre" value="" oninput="EspacioLetrasNumeros(this)" placeholder="Ingrese el nombre de la revista" class="form-control" required>
        </div>
        <div class="mb-3">
            <label for="fecha_publicacion" class="form-label"><b>Fecha publicación:</b></label>
            <input id="fecha_publicacion" type="date" name="fecha_publicacion" value=""  placeholder="" class="form-control" required>
        </div>
        <div class="mb-3">
            <label for="issn" class="form-label"><b>Issn:</b></label>
            <input id="issn" type="text" name="issn" value=""  placeholder="Ingrese el issn" class="form-control" required>
        </div>

        <div class="mb-3">
            <label for="volumen" class="form-label"><b>Volumen:</b></label>
            <input id="volumen" type="text" name="volumen" value=""  placeholder="Ingrese el volumen de la revista" class="form-control" required>
        </div>

        <div class="mb-3">
            <label for="numero" class="form-label"><b>Número:</b></label>
            <input id="numero" type="text" name="numero" value=""  placeholder="Ingrese el numero de la revista" class="form-control" required>
        </div>

<div class="">

        <label for="fotografia"> <b>Fotografía: </b> </label>
        <input type="file" name="logo" id="logo" value="" class="form-control" required accept="image/*"><br>
        <br>
      </div>


        <div class="row justify-content-end">
            <div class="col-auto">
                <button type="submit" name="button" class="btn btn-success">
                    <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbsp;Guardar&nbsp;
                </button>
            </div>
            <div class="col-auto">
                <a class="btn btn-danger" href="<?php echo site_url('revistas/index') ?>">
                    <i class="fa-solid fa-xmark fa-spin"></i>&nbsp;Cancelar&nbsp;
                </a>
            </div>
        </div>
    </form>
</div>
</div>

</div>

</div>
</div>
<script>
function eliminarRegistro(url) {
              Swal.fire({
                  title: '¿Estas seguro de eliminar este registro?',
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: '¡Sí, eliminalo!',
                  cancelButtonText: 'Cancelar'
              }).then((result) => {
                  if (result.isConfirmed) {
                      // Si el usuario confirma la eliminación, redireccionamos a la URL especificada
                      window.location.href = url;
                  } else {
                      // Si el usuario cancela, mostramos un mensaje de cancelación
                      Swal.fire(
                          'Cancelado',
                          'Tu registro no ha sido eliminado :P',
                          'error'
                      );
                  }
              });
          }
      </script>
