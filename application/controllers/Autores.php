<?php

/**
 *
 */
class Autores extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->model("Autor");
    //deshabilitando errores y advertencias de PHP
    error_reporting(0);

  }
  public function index(){

    $data["listadoAutores"]=
                    $this->Autor->consultarTodos();
    $this->load->view("header");
    $this->load->view("autores/index",$data);
    $this->load->view("footer");
  }
  public function nuevo(){
    $this->load->view("header");
    $this->load->view("autores/index", $data);
    $this->load->view("footer");
}
  public function borrar($id){
    $this->Autor->eliminar($id);
    $this->session->set_flashdata("confirmacion", "Autor eliminado existosamente");
    redirect('autores/index');
}


public function guardarAutor() {
  // Obtener los datos del formulario
  $datos = array(
      'nombre' => $this->input->post('nombre'),
      'apellido' => $this->input->post('apellido'),
      'direccion' => $this->input->post('direccion'),
      'pais' => $this->input->post('pais'),
      'correo' => $this->input->post('correo'),
      'telefono' => $this->input->post('telefono')

  );

  // Llamar al método insertar del modelo Editorial para guardar la nueva editorial
  $this->Autor->insertar($datos);
  $this->session->set_flashdata("confirmacion","Autor guardado exitosamente");
  // Redireccionar de vuelta a la página de editoriales después de guardar
  redirect('autores/index');
}
public function editar($id){
      $this->load->model("Autor");
      $data["autorEditar"] = $this->Autor->obtenerPorId($id);
      $this->load->view("header");
      $this->load->view("autores/editar", $data);
      $this->load->view("footer");
  }

  public function actualizarAutor(){
      $id = $this->input->post("id");

      // Obtener los datos actuales del doctor
      $autorEditar = $this->Autor->obtenerPorId($id);

      $datosAutor = array(
        'nombre' => $this->input->post('nombre'),
        'apellido' => $this->input->post('apellido'),
        'direccion' => $this->input->post('direccion'),
        'pais' => $this->input->post('pais'),
        'correo' => $this->input->post('correo'),
        'telefono' => $this->input->post('telefono'),
      );

      // Actualizar los datos del doctor en la base de datos
      $this->Autor->actualizar($id, $datosAutor);

      // Flash message
        $this->session->set_flashdata("confirmacion", "Autor actualizado exitosamente");

      // Redireccionar a la página de lista de doctores
      redirect('autores/index');
  }










}



 ?>
