<?php

/**
 *
 */
class Editoriales extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->model("Editorial");
    //deshabilitando errores y advertencias de PHP
    error_reporting(0);

  }
  public function index(){

    $data["listadoEditoriales"]=
                    $this->Editorial->consultarTodos();
    $this->load->view("header");
    $this->load->view("editoriales/index",$data);
    $this->load->view("footer");
  }
  public function nuevo(){
    $this->load->view("header");
    $this->load->view("editoriales/index", $data);
    $this->load->view("footer");
}
  public function borrar($id){
    $this->Editorial->eliminar($id);
    $this->session->set_flashdata("confirmacion", "Editorial eliminado existosamente");
    redirect('editoriales/index');
}


public function guardarEditorial() {
  // Obtener los datos del formulario
  $datos = array(
      'nombre' => $this->input->post('nombre'),
      'telefono' => $this->input->post('telefono'),
      'correo' => $this->input->post('correo')
  );

  // Llamar al método insertar del modelo Editorial para guardar la nueva editorial
  $this->Editorial->insertar($datos);
  $this->session->set_flashdata("confirmacion","Editorial guardada exitosamente");
  // Redireccionar de vuelta a la página de editoriales después de guardar
  redirect('editoriales/index');
}
public function editar($id){
      $this->load->model("Editorial");
      $data["editorialEditar"] = $this->Editorial->obtenerPorId($id);
      $this->load->view("header");
      $this->load->view("editoriales/editar", $data);
      $this->load->view("footer");
  }

  public function actualizarEditorial(){
      $id = $this->input->post("id");

      // Obtener los datos actuales del doctor
      $editorialEditar = $this->Editorial->obtenerPorId($id);

      $datosEditorial = array(
          "nombre" => $this->input->post("nombre"),
          "telefono" => $this->input->post("telefono"),
          "correo" => $this->input->post("correo"),
      );

      // Actualizar los datos del doctor en la base de datos
      $this->Editorial->actualizar($id, $datosEditorial);

      // Flash message
        $this->session->set_flashdata("confirmacion", "Entidad actualizada exitosamente");

      // Redireccionar a la página de lista de doctores
      redirect('editoriales/index');
  }










}



 ?>
