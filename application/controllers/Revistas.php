<?php

/**
 *
 */
class Revistas extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model("Revista");
    error_reporting(0);

  }
  public function index(){
      $data["listadoRevistas"] = $this->Revista->consultarTodos();
      $data["listadoRevistas"] = $this->Revista->consultarTodosConEditoriales();
      $data["listadoEditoriales"] = $this->Revista->obtenerListadoEditoriales();


      $this->load->view("header");
      $this->load->view("revistas/index",$data);
      $this->load->view("footer");
  }

  public function borrar($id){
    $this->Revista->eliminar($id);
    $this->session->set_flashdata("confirmacion", "Revista eliminada existosamente");
    redirect('revistas/index');
}



  public function nuevo(){
      $this->load->view("header");
      $this->load->view("revistas/nuevo");
      $this->load->view("footer");
  }

  //Renderizar el formulario de edicion
  public function editar($id){
      $this->load->model("Revista");

      $data["listadoEditoriales"] = $this->Revista->obtenerListadoEditoriales();
      $data["revistaEditar"] = $this->Revista->obtenerPorId($id);
      $this->load->view("header");
      $this->load->view("revistas/editar", $data);
      $this->load->view("footer");
  }

#aqui me quede ----------------------------------------------------------

public function actualizarRevista(){
    $id = $this->input->post("id");

    // Obtener los datos actuales del revista
    $revistaEditar = $this->Revista->obtenerPorId($id);

    // Proceso de subida de archivo si se proporciona una nueva imagen
    $nombre_archivo_subido = $revistaEditar->logo; // Por defecto, mantén el nombre actual de la imagen
    if (!empty($_FILES['fotografia_nueva']['name'])) { // Verifica si se ha proporcionado un nuevo archivo de imagen
        // Configuración de la subida de archivo
        $config['upload_path'] = APPPATH . '../uploads/revistas/';
        $config['allowed_types'] = 'jpeg|jpg|png';
        $config['max_size'] = 5 * 1024; // 5 MB

        // Genera un nombre aleatorio para el archivo
        $nombre_aleatorio = "revista_" . time() * rand(100, 10000);
        $config['file_name'] = $nombre_aleatorio;

        // Carga la librería de subida de archivos con la configuración definida
        $this->load->library('upload', $config);

        // Intenta subir el archivo
        if ($this->upload->do_upload("fotografia_nueva")) {
            // Obtén los datos del archivo subido
            $dataArchivoSubido = $this->upload->data();
            // Captura el nombre del archivo subido
            $nombre_archivo_subido = $dataArchivoSubido["file_name"];
        } else {
            // Manejo de errores de subida de archivos aquí si es necesario
            $error = $this->upload->display_errors();
            // Puedes mostrar un mensaje de error o redirigir a una página de error
            // Por ejemplo: $this->session->set_flashdata("error", $error);
            // Luego, redirigir a la página de edición con el mensaje de error
            // Por ejemplo: redirect("revistas/editar/$id");
            return;
        }
    }

    // Obtener los datos actualizados del formulario
    $datosRevista = array(
        "nombre" => $this->input->post("nombre"),
        "fecha_publicacion" => $this->input->post("fecha_publicacion"),
        "issn" => $this->input->post("issn"),
        "volumen" => $this->input->post("volumen"),
        "numero" => $this->input->post("numero"),
        "fkid_edi" => $this->input->post("fkid_edi"),
        "logo" => $nombre_archivo_subido // Actualiza el nombre de la imagen
    );

    // Actualizar los datos del revista en la base de datos
    $this->Revista->actualizar($id, $datosRevista);

    // Flash message
    $this->session->set_flashdata("confirmacion", "Revista actualizada exitosamente");

    // Redireccionar a la página de lista de revistas
    redirect('revistas/index');
}



  //insertar hospitales
  public function guardarRevista(){
    /*INICIO PROCESO DE SUBIDA DE ARCHIVO*/
    $config['upload_path']=APPPATH.'../uploads/revistas/'; //ruta de subida de archivos
    $config['allowed_types']='jpeg|jpg|png';//tipo de archivos permitidos
    $config['max_size']=5*1024;//definir el peso maximo de subida (5MB)
    $nombre_aleatorio="revista_".time()*rand(100,10000);//creando un nombre aleatorio
    $config['file_name']=$nombre_aleatorio;//asignando el nombre al archivo subido
    $this->load->library('upload',$config);//cargando la libreria UPLOAD
    if($this->upload->do_upload("logo")){ //intentando subir el archivo
       $dataArchivoSubido=$this->upload->data();//capturando informacion del archivo subido
       $nombre_archivo_subido=$dataArchivoSubido["file_name"];//obteniendo el nombre del archivo
    }else{
      $nombre_archivo_subido="";//Cuando no se sube el archivo el nombre queda VACIO
    }
    $datosNuevosRevista=array(
      "nombre" => $this->input->post("nombre"),
      "fecha_publicacion" => $this->input->post("fecha_publicacion"),
      "issn" => $this->input->post("issn"),
      "volumen" => $this->input->post("volumen"),
      "numero" => $this->input->post("numero"),
      "fkid_edi" => $this->input->post("fkid_edi"),
      "logo" => $nombre_archivo_subido

    );
    $this->Revista->insertar($datosNuevosRevista);
      //flash crear una sesion tipo flash
      $this->session->set_flashdata("confirmacion","Revista guardada exitosamente");
      redirect('revistas/index');
  }




}











 ?>
