<?php

/**
 *
 */
class Autorias extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model("Autoria");
    error_reporting(0);

  }
  public function index(){
    $data["listadoAutoriasConAutores"] = $this->Autoria->consultarTodosConAutores();
    $data["listadoAutoriasConArticulos"] = $this->Autoria->consultarTodosConArticulos();
    $data["listadoAutores"] = $this->Autoria->obtenerListadoAutores();
    $data["listadoArticulos"] = $this->Autoria->obtenerListadoArticulos();

    // Fusionar los resultados en una sola variable
    $data["listadoAutorias"] = $this->Autoria->consultarTodosConAutoresYArticulos();

        $this->load->view("header");
        $this->load->view("autorias/index",$data);
        $this->load->view("footer");
    }

  public function borrar($id){
    $this->Autoria->eliminar($id);
    $this->session->set_flashdata("confirmacion", "Autoria eliminada existosamente");
    redirect('autorias/index');
}



  public function nuevo(){
      $this->load->view("header");
      $this->load->view("autorias/nuevo");
      $this->load->view("footer");
  }

  //Renderizar el formulario de edicion
  public function editar($id){
      $this->load->model("Autoria");

      $data["listadoAutores"] = $this->Autoria->obtenerListadoAutores();
      $data["listadoArticulos"] = $this->Autoria->obtenerListadoArticulos();
      $data["autoriaEditar"] = $this->Autoria->obtenerPorId($id);

      $this->load->view("header");
      $this->load->view("autorias/editar", $data);
      $this->load->view("footer");
  }

#aqui me quede ----------------------------------------------------------

public function actualizarAutoria(){
    $id = $this->input->post("id");

    // Obtener los datos actuales de la autoría
    $autorEditar = $this->Autoria->obtenerPorId($id);

    // Obtener los datos actualizados del formulario
    $datosAutoria = array(
        "fkid_arti" => $this->input->post("fkid_arti"),
        "fkid_autor" => $this->input->post("fkid_autor")
    );

    // Actualizar los datos de la autoría en la base de datos
    $this->Autoria->actualizar($id, $datosAutoria);

    // Flash message
    $this->session->set_flashdata("confirmacion", "Autoría actualizada exitosamente");

    // Redireccionar a la página de lista de revistas
    redirect('autorias/index');
}



  //insertar hospitales
  public function guardarAutoria(){
      $datosNuevosAutoria=array(
        "fkid_arti" => $this->input->post("fkid_arti"),
        "fkid_autor" => $this->input->post("fkid_autor")
      );

      $this->Autoria->insertar($datosNuevosAutoria);

      // Flash message
      $this->session->set_flashdata("confirmacion","Autoria guardada exitosamente");

      // Redireccionar a la página de lista de revistas
      redirect('autorias/index');
  }




}











 ?>
