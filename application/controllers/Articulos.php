<?php

/**
 *
 */
class Articulos extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model("Articulo");
    error_reporting(0);

  }
  public function index(){
    $data["listadoArticulos"] = $this->Articulo->consultarTodos();
    $data["listadoArticulos"] = $this->Articulo->consultarTodosConEditoriales();
    $data["listadoRevistas"] = $this->Articulo->obtenerListadoRevistas();

      $this->load->view("header");
      $this->load->view("articulos/index",$data);
      $this->load->view("footer");
  }

  public function borrar($id){
    $this->Articulo->eliminar($id);
    $this->session->set_flashdata("confirmacion", "Articulo eliminado existosamente");
    redirect('articulos/index');
}



  public function nuevo(){
      $this->load->view("header");
      $this->load->view("articulos/nuevo");
      $this->load->view("footer");
  }

  //Renderizar el formulario de edicion
  public function editar($id){
      $this->load->model("Articulo");

      $data["listadoRevistas"] = $this->Articulo->obtenerListadoRevistas();
      $data["articuloEditar"] = $this->Articulo->obtenerPorId($id);



      $this->load->view("header");
      $this->load->view("articulos/editar", $data);
      $this->load->view("footer");
  }

#aqui me quede ----------------------------------------------------------

public function actualizarArticulo(){
    $id = $this->input->post("id");

    // Obtener los datos actualizados del formulario
    $datosArticulo = array(
        "nombre" => $this->input->post("nombre"),
        "forma" => $this->input->post("forma"),
        "formato" => $this->input->post("formato"),
        "direccion_electronica" => $this->input->post("direccion_electronica"),
        "num_paginas" => $this->input->post("num_paginas"),
        "fkid_revis" => $this->input->post("fkid_revis")
    );

    // Actualizar los datos del revista en la base de datos
    $this->Articulo->actualizar($id, $datosArticulo);

    // Flash message
    $this->session->set_flashdata("confirmacion", "Articulo actualizada exitosamente");

    // Redireccionar a la página de lista de revistas
    redirect('articulos/index');
}


public function guardarArticulo(){
    $datosNuevosArticulo = array(
      "nombre" => $this->input->post("nombre"),
      "forma" => $this->input->post("forma"),
      "formato" => $this->input->post("formato"),
      "direccion_electronica" => $this->input->post("direccion_electronica"),
      "num_paginas" => $this->input->post("num_paginas"),
      "fkid_revis" => $this->input->post("fkid_revis")
    );

    $this->Articulo->insertar($datosNuevosArticulo);

    // Flash message
    $this->session->set_flashdata("confirmacion","Articulo guardado exitosamente");

    redirect('articulos/index');
}




}











 ?>
