<?php

/**
 *
 */
class Directores extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->model("Director");
    //deshabilitando errores y advertencias de PHP
    error_reporting(0);

  }
  public function index(){

    $data["listadoDirectores"]=
                    $this->Director->consultarTodos();
    $data["listadoDirectores"] = $this->Director->consultarTodosConEditoriales();
      $data["listadoEditoriales"] = $this->Director->obtenerListadoEditoriales();
    $this->load->view("header");
    $this->load->view("directores/index",$data);
    $this->load->view("footer");
  }
  public function borrar($id){
    $this->Director->eliminar($id);
    $this->session->set_flashdata("confirmacion", "Director eliminado existosamente");
    redirect('directores/index');
}



  //Renderizando hospitales
  public function nuevo(){
      $data["listadoEditoriales"] = $this->Director->obtenerListadoEditoriales();
      $this->load->view("header");
      $this->load->view("directores/index", $data);
      $this->load->view("footer");
  }

  //Renderizar el formulario de edicion
  public function editar($id){
      $this->load->model("Director");
      $data["listadoEditoriales"] = $this->Director->obtenerListadoEditoriales();
      $data["directorEditar"] = $this->Director->obtenerPorId($id);
      $this->load->view("header");
      $this->load->view("directores/editar", $data);
      $this->load->view("footer");
  }



  //insertar hospitales
  public function guardarDirector(){
    /*INICIO PROCESO DE SUBIDA DE ARCHIVO*/
    $config['upload_path']=APPPATH.'../uploads/directores/'; //ruta de subida de archivos
    $config['allowed_types']='jpeg|jpg|png';//tipo de archivos permitidos
    $config['max_size']=5*1024;//definir el peso maximo de subida (5MB)
    $nombre_aleatorio="firma_".time()*rand(100,10000);//creando un nombre aleatorio
    $config['file_name']=$nombre_aleatorio;//asignando el nombre al archivo subido
    $this->load->library('upload',$config);//cargando la libreria UPLOAD
    if($this->upload->do_upload("fotografia")){ //intentando subir el archivo
       $dataArchivoSubido=$this->upload->data();//capturando informacion del archivo subido
       $nombre_archivo_subido=$dataArchivoSubido["file_name"];//obteniendo el nombre del archivo
    }else{
      $nombre_archivo_subido="";//Cuando no se sube el archivo el nombre queda VACIO
    }
    $datosNuevosDirector=array(
      "cedula" => $this->input->post("cedula"),
      "nombre" => $this->input->post("nombre"),
      "apellido" => $this->input->post("apellido"),
      "telefono" => $this->input->post("telefono"),
      "correo" => $this->input->post("correo"),
      "fotografia" => $nombre_archivo_subido, // Se actualiza el nombre del archivo
      "fkid_edi" => $this->input->post("fkid_edi")
    );
    $this->Director->insertar($datosNuevosDirector);
      //flash crear una sesion tipo flash
      $this->session->set_flashdata("confirmacion","director guardado exitosamente");
      redirect('directores/index');
  }

  public function actualizarDirector()
{
 $id = $this->input->post("id");

 // Obtener los datos actuales de la agencia
 $directorEditar = $this->Director->obtenerPorId($id);

 // Obtener los datos actualizados del formulario
 $datosDirector = array(
   "cedula" => $this->input->post("cedula"),
   "nombre" => $this->input->post("nombre"),
   "apellido" => $this->input->post("apellido"),
   "telefono" => $this->input->post("telefono"),
   "correo" => $this->input->post("correo"),
   "fkid_edi" => $this->input->post("fkid_edi")
 );

 // Proceso de subida de la fotografía si se proporciona una nueva imagen
 if (!empty($_FILES['fotografia_nueva']['name'])) {
     $config['upload_path'] = APPPATH . '../uploads/directores/';
     $config['allowed_types'] = 'jpeg|jpg|png';
     $config['max_size'] = 5 * 1024; // 5 MB

     $nombre_aleatorio = "fotografia_" . time() * rand(100, 10000);
     $config['file_name'] = $nombre_aleatorio;

     $this->load->library('upload', $config);

     if ($this->upload->do_upload("fotografia_nueva")) {
         $dataArchivoSubido = $this->upload->data();
         $datosCajero["fotografia"] = $dataArchivoSubido["file_name"];
     } else {
         $error = $this->upload->display_errors();
         return;
     }
 } else {
     // Si no se proporciona una nueva imagen, mantener la imagen actual
     $datosCajero["fotografia"] = $directorEditar->fotografia;
 }

 // Actualizar la agencia con los nuevos datos
 $this->Director->actualizar($id, $datosDirector);

 // Flash message
 $this->session->set_flashdata("confirmacion", "Director actualizado exitosamente");

 // Redireccionar a la página de lista de agencias
 redirect('directores/index');
}





}



 ?>
